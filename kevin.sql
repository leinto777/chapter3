-- Perintah Create Database
CREATE DATABASE challenge_chapter3_kevin;

-- Perintah Create Tables
CREATE TABLE users_game (
    id_user_game BIGSERIAL PRIMARY KEY,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL
);

CREATE TABLE user_game_biodata (
    id_user_game_bio BIGSERIAL PRIMARY KEY,
    bio TEXT,
    id_user_game INT
);

CREATE TABLE user_game_history (
    id_user_game_history BIGSERIAL PRIMARY KEY,
    score INT,
    last_online TIMESTAMP,
    id_user_game INT
);

-- Query Database
INSERT INTO users_game (username, password) 
VALUES
    ('leinto777', 'inipasw123'),
    ('the_killer', 'thekill123'),
    ('the_losers', 'losers123'),
    ('fuzzy12', 'fuzz289'),
    ('buzzlightyear', 'woody898');

INSERT INTO user_game_biodata (bio, id_user_game) 
VALUES
    ('We are gamers', 1),
    ('Nobody can hurt us', 2),
    ('Cant touch this', 3),
    ('Boom boom boom', 4),
    ('To infinity and beyond', 5);

INSERT INTO user_game_history (score, id_user_game) 
VALUES
    (1050, 1),
    (2060, 2),
    (3060, 3),
    (1070, 1),
    (3060, 4),
    (1360, 5),
    (4500, 2),
    (9000, 3),
    (2500, 5),
    (4000, 4);

-- DML
UPDATE user_game_biodata
SET bio = "We are legion"
WHERE id_user_game_bio = 1;

DELETE 
FROM user_game_biodata
WHERE id_user_game_bio = 3;




